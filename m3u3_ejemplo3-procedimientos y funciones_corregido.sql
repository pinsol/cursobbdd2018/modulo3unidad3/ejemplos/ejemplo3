﻿USE m3u3ejemplo3;

/* Ejercicio 2: Realizar una función que reciba como argumentos:
                  - Base de un triangulo
                  - Altura de un triangulo
                Debe devolver el cálculo del área del triángulo.

                Como utilizarlo: 
                SELECT areaTr(base,altura);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION areaTr(base float, altura float)
RETURNS float
COMMENT 'Esta funcion calcula el area del triangulo. Recibe los parametros base y altura del triangulo'
BEGIN
  DECLARE areaTr float DEFAULT 0;

  SET areaTr=(base*altura)/2;

  RETURN areaTr;
END //
DELIMITER;

SELECT areaTr(2,5);


/* Ejercicio 3: Realizar una función que reciba como argumentos:
                  - Base de un triangulo
                  - Lado2 de un triangulo
                  - Lado3 de un triangulo
                Debe devolver el cálculo del perímetro del triángulo.


                Como utilizarlo: 
                SELECT perimTr(base,lado2,lado3);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION perimTr(base float, lado2 float, lado3 float)
RETURNS float
COMMENT 'Esta funcion calcula el perimetro del triangulo. Recibe los parametros base y los otros lados del triangulo'
BEGIN
  DECLARE perimTr float DEFAULT 0;

  SET perimTr=base+lado2+lado3;

  RETURN perimTr;
END //
DELIMITER;

SELECT perimTr(2,5,6);


/* Ejercicio 4: Realizar un procedimiento almacenado que cuando le llames como argumentos:
                  - Id1: id inicial
                  - Id2: id final
                Actualice el área y el perímetro de los triángulos
                (utilizando las funciones realizadas) que estén comprendidos entre los id
                pasados.


                Como utilizarlo:
                CALL actTr(id1,id2); 
                SELECT * FROM triangulos;
*/
DELIMITER //
CREATE OR REPLACE PROCEDURE actTr(IN id1 int, IN id2 int)
  COMMENT 'Este procedimiento actualizara entre los valores introducidos el area y el perimetro de los triangulos'
  BEGIN
    UPDATE triangulos SET area=NULL;
    UPDATE triangulos SET perimetro=NULL;
    UPDATE triangulos SET area=areaTr(base,altura) WHERE id BETWEEN id1 AND id2;
    UPDATE triangulos SET perimetro=perimTr(base,lado2,lado3) WHERE id BETWEEN id1 AND id2;    
  END //
    DELIMITER;

CALL actTr(4,9);
SELECT * FROM triangulos;


/* Ejercicio 5: Realizar una función que reciba como argumentos:
                  - Lado de un cuadrado
                Debe devolver el cálculo del área


                Como utilizarlo:
                SELECT areaSq(lado);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION areaSq(lado float)
RETURNS float
  COMMENT 'Esta funcion calcula el area del cuadrado. Recibe el parametro del lado del cuadrado'
  BEGIN
    DECLARE areaSq float DEFAULT 0;
  
    SET areaSq=lado*lado;
  
    RETURN areaSq;
  END //
DELIMITER;

SELECT areaSq(2);


/* Ejercicio 6: Realizar una función que reciba como argumentos:
                  - Lado de un cuadrado
                Debe devolver el cálculo del perímetro
                
                Como utilizarlo:
                SELECT perimSq(lado);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION perimSq(lado float)
RETURNS float
COMMENT 'Esta funcion calcula el perimetro del cuadrado. Recibe el parametro del lado del cuadrado'
  BEGIN
  DECLARE perimSq float DEFAULT 0;

  SET perimSq=lado*4;

  RETURN perimSq;
END //
DELIMITER;

SELECT perimSq(2);


/* Ejercicio 7: Realizar un procedimiento almacenado que cuando le llames como argumentos:
                  - Id1: id inicial
                  - Id2: id final
                Actualice el área y el perímetro de los cuadrados
                (utilizando las funciones realizadas) que estén comprendidos entre los id
                pasados.


                Como utilizarlo:
                CALL actSq(id1,id2); 
                SELECT * FROM cuadrados;
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE actSq(IN id1 int, IN id2 int)
  COMMENT 'Este procedimiento actualizara entre los valores introducidos el area y el perimetro de los cuadrados'
  BEGIN
    UPDATE cuadrados SET area=NULL;
    UPDATE cuadrados SET perimetro=NULL;
    UPDATE cuadrados SET area=areaSq(lado) WHERE id BETWEEN id1 AND id2;
    UPDATE cuadrados SET perimetro=perimSq(lado) WHERE id BETWEEN id1 AND id2;    
  END //
    DELIMITER;

CALL actSq(4,9);
SELECT * FROM cuadrados;


/* Ejercicio 8: Realizar una función que reciba como argumentos:
                  - radio
                Debe devolver el cálculo del área


                Como utilizarlo:
                SELECT areaCircle(lado);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION areaCircle(radio float)
RETURNS float
  COMMENT 'Esta funcion calcula el area del circulo. Recibe el parametro del radio del circulo'
  BEGIN
    DECLARE areaCircle float DEFAULT 0;
  
    SET areaCircle=PI()*POW(radio,2);

    RETURN areaCircle;
  END //
DELIMITER;

SELECT areaCircle(6);


/* Ejercicio 9: Realizar una función que reciba como argumentos:
                  - radio
                Debe devolver el cálculo del perímetro
                
                Como utilizarlo:
                SELECT perimCircle(radio);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION perimCircle(radio float)
RETURNS float
COMMENT 'Esta funcion calcula el perimetro del circulo. Recibe el parametro del radio del circulo'
  BEGIN
  DECLARE perimCircle float DEFAULT 0;

  SET perimCircle=2*PI()*radio;

  RETURN perimCircle;
END //
DELIMITER;

SELECT perimCircle(2);


/* Ejercicio 10: Realizar un procedimiento almacenado que cuando le llames como argumentos:
                  - Id1: id inicial
                  - Id2: id final
                  - Tipo: puede ser a, b o null
                Actualice el área y el perímetro de los circulo (utilizando las funciones realizadas)
                que estén comprendidos entre los id pasados.


                Como utilizarlo:
                CALL actCircle(id1,id2,t); 
                SELECT * FROM circulo;
*/
DELIMITER //
  CREATE PROCEDURE actCircle(IN id1 int, IN id2 int, IN t char(1))
  COMMENT 'Este procedimiento actualizara entre los valores introducidos el area y el perimetro de los circulos'
  BEGIN
    UPDATE circulo SET area=NULL;
    UPDATE circulo SET perimetro=NULL;
    IF t IS NULL THEN 
      UPDATE circulo SET area=areaCircle(radio) WHERE (id BETWEEN id1 AND id2) AND tipo=t;
      UPDATE circulo SET perimetro=perimCircle(radio) WHERE (id BETWEEN id1 AND id2) AND tipo=t; 
    ELSE
       UPDATE circulo SET area=areaCircle(radio) WHERE id BETWEEN id1 AND id2;
       UPDATE circulo SET perimetro=perimCircle(radio) WHERE id BETWEEN id1 AND id2;
    END IF;
  END //
    DELIMITER;

CALL actCircle(4,10,'b');
SELECT * FROM circulo;



/* Ejercicio 11: Realizar una función que reciba como argumentos:
                  - base
                  - altura
                Debe devolver el cálculo del área


                Como utilizarlo:
                SELECT areaRectang(lado);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION areaRectang(lado1 float, lado2 float)
RETURNS float
  COMMENT 'Esta funcion calcula el area del rectangulo. Recibe los parametros de los lados desiguales del rectangulo'
  BEGIN
    DECLARE areaRectang float DEFAULT 0;
  
    SET areaRectang=lado1*lado2;
  
    RETURN areaRectang;
  END //
DELIMITER;

SELECT areaRectang(6,2);


/* Ejercicio 12: Realizar una función que reciba como argumentos:
                  - base
                  - altura
                Debe devolver el cálculo del perímetro
                
                Como utilizarlo:
                SELECT perimCircle(radio);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION perimRectang(lado1 float, lado2 float)
RETURNS float
COMMENT 'Esta funcion calcula el perimetro del rectangulo. Recibe los parametros de los lados desiguales del rectangulo'
  BEGIN
  DECLARE perimRectang float DEFAULT 0;

  SET perimRectang=(lado1*2)+(lado2*2);

  RETURN perimRectang;
END //
DELIMITER;

SELECT perimRectang(2,4);


/* Ejercicio 13: Realizar un procedimiento almacenado que cuando le llames como argumentos:
                  - Id1: id inicial
                  - Id2: id final
                Actualice el área y el perímetro de los rectangulos
                (utilizando las funciones realizadas) que estén comprendidos entre los id
                pasados.


                Como utilizarlo:
                CALL actCircle(id1,id2); 
                SELECT * FROM circulo;
*/
DELIMITER //
  CREATE PROCEDURE actRectang(IN id1 int, IN id2 int)
  COMMENT 'Este procedimiento actualizara entre los valores introducidos el area y el perimetro de los rectangulos'
  BEGIN
    UPDATE rectangulo SET area=NULL;
    UPDATE rectangulo SET perimetro=NULL;
    UPDATE rectangulo SET area=areaRectang(lado1,lado2) WHERE id BETWEEN id1 AND id2;
    UPDATE rectangulo SET perimetro=perimRectang(lado1,lado2) WHERE id BETWEEN id1 AND id2;    
  END //
    DELIMITER;

CALL actRectang(4,9);
SELECT * FROM rectangulo;


/* Ejercicio 14: Realizar una función que reciba como argumentos:
                  - nota1
                  - nota2
                  - nota3
                  - nota4
                Debe devolver el cálculo de la nota media


                Como utilizarlo:
                SELECT media(nota1,nota2,nota3,nota4);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION media(nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
  COMMENT 'Esta funcion calcula la nota media de cuatro notas. Recibe los parametros de las notas de un alumno'
  BEGIN
    DECLARE media float;
  
    SET media=(nota1+nota2+nota3+nota4)/4;
  
    RETURN media;
  END //
DELIMITER;

SELECT media(6,4,5,7);


/* Ejercicio 15: Realizar una función que reciba como argumentos:
                  - nota1
                  - nota2
                  - nota3
                  - nota4
                Debe devolver el cálculo de la nota minima


                Como utilizarlo:
                SELECT minScore(nota1,nota2,nota3,nota4);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION minScore(nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
  COMMENT 'Esta funcion calcula la nota minima de cuatro notas. Recibe los parametros de las notas de un alumno'
  BEGIN
    DECLARE minScore float;
  
    SET minScore=LEAST(nota1,nota2,nota3,nota4);
  
    RETURN minScore;
  END //
DELIMITER;

SELECT minScore(6,4,5,7);


/* Ejercicio 16: Realizar una función que reciba como argumentos:
                  - nota1
                  - nota2
                  - nota3
                  - nota4
                Debe devolver el cálculo de la nota maxima


                Como utilizarlo:
                SELECT maxScore(nota1,nota2,nota3,nota4);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION maxScore(nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
  COMMENT 'Esta funcion calcula la nota maxima de cuatro notas. Recibe los parametros de las notas de un alumno'
  BEGIN
    DECLARE maxScore float;
  
    SET maxScore=GREATEST(nota1,nota2,nota3,nota4);
  
    RETURN maxScore;
  END //
DELIMITER;

SELECT maxScore(6,4,5,7);


/* Ejercicio 17: Realizar una función que reciba como argumentos:
                  - nota1
                  - nota2
                  - nota3
                  - nota4
                Debe devolver el cálculo de la moda


                Como utilizarlo:
                SELECT moda(nota1,nota2,nota3,nota4);
*/
DELIMITER //

CREATE OR REPLACE FUNCTION moda(nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
  COMMENT 'Esta funcion calcula la moda o nota mas repetida de cuatro notas. Recibe los parametros de las notas de un alumno'
  BEGIN
    DECLARE moda float;
    CREATE OR REPLACE TEMPORARY TABLE tablamodas(
      id int AUTO_INCREMENT PRIMARY KEY,
      notas float
      );

    INSERT INTO tablamodas(notas) VALUES (nota1),(nota2),(nota3),(nota4);

    SELECT notas INTO moda FROM tablamodas GROUP BY notas ORDER BY COUNT(*) DESC LIMIT 1;
 
    RETURN moda;
  END //
DELIMITER;

SELECT moda(6,5,5,7);


/* Ejercicio 18: Realizar un procedimiento almacenado que cuando le llames como argumentos:
                  - Id1: id inicial
                  - Id2: id final
                Actualice las notas minima, maxima, media y moda de los alumnos
                (utilizando las funciones realizadas) que estén comprendidos entre los id
                pasados y por grupos.


                Como utilizarlo:
                CALL actAlumn(id1,id2); 
                SELECT * FROM alumnos;
*/
DELIMITER //
  CREATE OR REPLACE PROCEDURE actAlumn(IN id1 int, IN id2 int)
  COMMENT 'Este procedimiento actualizara entre los valores introducidos moda, media, maximo, minimo y moda'
  BEGIN
    UPDATE alumnos SET media=NULL;
    UPDATE alumnos SET max=NULL;
    UPDATE alumnos SET min=NULL;
    UPDATE alumnos SET moda=NULL;
    UPDATE alumnos SET media=media(nota1,nota2,nota3,nota4) WHERE id BETWEEN id1 AND id2;
    UPDATE alumnos SET max=maxScore(nota1,nota2,nota3,nota4) WHERE id BETWEEN id1 AND id2;
    UPDATE alumnos SET min=minScore(nota1,nota2,nota3,nota4) WHERE id BETWEEN id1 AND id2;
    UPDATE alumnos SET moda=moda(nota1,nota2,nota3,nota4) WHERE id BETWEEN id1 AND id2; 
    
    UPDATE grupos
      SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=1
      AND id BETWEEN id1 AND id2)
      WHERE id1;
     
    UPDATE grupos
      SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=2
      AND id BETWEEN id1 AND id2)
      WHERE id2;
  END //
    DELIMITER;

CALL actAlumn(6,9);
SELECT * FROM alumnos;


/* Ejercicio 19: Realizar un procedimiento almacenado que le pasas como argumento un id
                 y te calcula la conversión de unidades de todos los registros que estén detrás de ese id.
                 Si está escrito los cm calculara m, km y pulgadas.
                 Si está escrito en m calculara cm, km y pulgadas y así consecutivamente


                Como utilizarlo:
                CALL conver(arg1); 
                SELECT * FROM conversion;
*/
DELIMITER //
  CREATE PROCEDURE conver(IN id1 int)
  COMMENT 'Este procedimiento actualizara entre los valores introducidos el area y el perimetro de los rectangulos'
  BEGIN
    UPDATE conversion
      SET
        m=cm/100,
        km=cm/100000,
        pulgadas=cm/0.393701
      WHERE cm IS NOT NULL;
    
    UPDATE conversion
      SET
        cm=m*100,
        km=m/1000,
        pulgadas=cm/0.393701
      WHERE m IS NOT NULL AND cm IS NOT NULL;
    
    UPDATE conversion
      SET
        cm=km/100000,
        m=km*1000,
        pulgadas=cm/0.393701
      WHERE km IS NOT NULL AND cm IS NULL;
    
     UPDATE conversion
      SET
        cm=pulgadas*2.54,
        m=pulgadas*254,
        km=pulgadas*254000
      WHERE pulgadas IS NOT NULL AND cm IS NULL;
  END //
    DELIMITER;

CALL conver(4);
SELECT * FROM conversion;